package com.hotelbeds.supplierintegrations.hackertest.detector;

import java.time.LocalDateTime;
import java.time.ZoneId;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import com.hotelbeds.supplierintegrations.hackertest.detector.validators.reviewers.SuspiciousActivityReviewerImpl;
import com.hotelbeds.supplierintegrations.hackertest.detector.vo.LoginData;
import com.hotelbeds.supplierintegrations.hackertest.detector.vo.LoginResult;

class SuspiciousActivityReviewerImplTests {
	@Test
	void with_no_limit_then_return_false() {
		LoginData loginData = generateOperationData("80.238.9.179", LocalDateTime.now());
		
		int operationCounter = 1;
		while (operationCounter++ < (SuspiciousActivityReviewerImpl.MAX_NUMBER_OF_FILURED_LOGINS - 2)) {
			new SuspiciousActivityReviewerImpl().isSuspiciousActivity(loginData);
		}
			
		Assertions.assertFalse(new SuspiciousActivityReviewerImpl().isSuspiciousActivity(loginData));
	}
	
	@Test
	void with_limit_but_no_on_time_then_return_false() {
		LocalDateTime ahora = LocalDateTime.now();
		LoginData loginData = generateOperationData("80.238.9.180", ahora);
		
		int operationCounter = 1;
		while (operationCounter++ < SuspiciousActivityReviewerImpl.MAX_NUMBER_OF_FILURED_LOGINS) {
			new SuspiciousActivityReviewerImpl().isSuspiciousActivity(loginData);
		}
		LocalDateTime momentoValido = ahora.plusMinutes(SuspiciousActivityReviewerImpl.DIFF_IN_MINUTES + 1);
		loginData.setMoment(momentoValido.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli());
		
		Assertions.assertFalse(new SuspiciousActivityReviewerImpl().isSuspiciousActivity(loginData));
	}
	
	@Test
	void with_limit_and_on_time_then_return_true() {
		LoginData loginData = generateOperationData("80.238.9.181", LocalDateTime.now());
		
		int operationCounter = 1;
		while (operationCounter++ < SuspiciousActivityReviewerImpl.MAX_NUMBER_OF_FILURED_LOGINS) {
			new SuspiciousActivityReviewerImpl().isSuspiciousActivity(loginData);
		}
		
		Assertions.assertTrue(new SuspiciousActivityReviewerImpl().isSuspiciousActivity(loginData));
	}
	
	@Test
	void with_more_of_limit_and_on_time_then_return_true() {
		LoginData loginData = generateOperationData("80.238.9.182", LocalDateTime.now());
		
		int operationCounter = 1;
		while (operationCounter++ <= SuspiciousActivityReviewerImpl.MAX_NUMBER_OF_FILURED_LOGINS) {
			new SuspiciousActivityReviewerImpl().isSuspiciousActivity(loginData);
		}
		
		Assertions.assertTrue(new SuspiciousActivityReviewerImpl().isSuspiciousActivity(loginData));
	}
	
	private LoginData generateOperationData(String ip, LocalDateTime momentoOperacion) {
		LoginData data = new LoginData();
		data.setIp(ip);
		data.setMoment(momentoOperacion.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli());
		data.setResult(LoginResult.FAILURE);
		data.setUser("Will.Smith");
		return data;
	}
}
