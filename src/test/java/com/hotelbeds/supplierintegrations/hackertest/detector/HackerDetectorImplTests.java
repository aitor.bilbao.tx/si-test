package com.hotelbeds.supplierintegrations.hackertest.detector;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import com.hotelbeds.supplierintegrations.hackertest.detector.validators.ValidatorDetectorParams;
import com.hotelbeds.supplierintegrations.hackertest.detector.validators.reviewers.SuspiciousActivityReviewerImpl;
import com.hotelbeds.supplierintegrations.hackertest.detector.vo.LoginData;

/**
 * Test for HackerDetectorImpl
 */
class HackerDetectorImplTests {
	@Test
	void with_login_ok_then_return_null() {
		ValidatorDetectorParams validator = mock(ValidatorDetectorParams.class);
		Mockito.lenient().when(validator.validate(anyString())).thenReturn(true);
		SuspiciousActivityReviewerImpl revisor = mock(SuspiciousActivityReviewerImpl.class);

		Assertions.assertNull(new HackerDetectorImpl(validator, revisor)
				.parseLine("80.238.9.179,133612947,SIGNIN_SUCCESS,Will.Smith"));
	}

	@Test
	void with_login_ko_and_no_suspicious_activity_return_null() {
		ValidatorDetectorParams validator = mock(ValidatorDetectorParams.class);
		Mockito.lenient().when(validator.validate(anyString())).thenReturn(true);
		SuspiciousActivityReviewerImpl revisor = mock(SuspiciousActivityReviewerImpl.class);
		Mockito.lenient().when(revisor.isSuspiciousActivity(any(LoginData.class))).thenReturn(false);

		Assertions.assertNull(new HackerDetectorImpl(validator, revisor)
				.parseLine("80.238.9.179,133612947,SIGNIN_FAILURE,Will.Smith"));
	}

	@Test
	void with_login_ko_and_suspicious_activity_return_ip() {
		ValidatorDetectorParams validator = mock(ValidatorDetectorParams.class);
		Mockito.lenient().when(validator.validate(anyString())).thenReturn(true);
		SuspiciousActivityReviewerImpl revisor = mock(SuspiciousActivityReviewerImpl.class);
		Mockito.lenient().when(revisor.isSuspiciousActivity(any(LoginData.class))).thenReturn(true);

		Assertions.assertEquals("80.238.9.179", new HackerDetectorImpl(validator, revisor)
				.parseLine("80.238.9.179,133612947,SIGNIN_FAILURE,Will.Smith"));
	}
}
