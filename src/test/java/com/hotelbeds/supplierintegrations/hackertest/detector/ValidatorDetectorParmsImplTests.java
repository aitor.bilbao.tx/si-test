package com.hotelbeds.supplierintegrations.hackertest.detector;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import com.hotelbeds.supplierintegrations.hackertest.detector.validators.ValidatorDetectorParamsImpl;

class ValidatorDetectorParmsImplTests {
	@Test
	void with_one_incorrect_param_then_throw_exception() {
		Assertions.assertThrows(IllegalArgumentException.class, () -> {
			new ValidatorDetectorParamsImpl().validate("one");
		});
	}
	
	@Test
	void with_incorrect_ip_then_throw_exception() {
		Assertions.assertThrows(IllegalArgumentException.class, () -> {
			new ValidatorDetectorParamsImpl().validate("ipErronea,1659547135,SIGNIN_SUCCESS,Will.Smith");
		});
	}
	
	@Test
	void with_incorrect_data_format_then_throw_exception() {
		Assertions.assertThrows(IllegalArgumentException.class, () -> {
			new ValidatorDetectorParamsImpl().validate("80.238.9.179,dateErronea,SIGNIN_SUCCESS,Will.Smith");
		});
	}

	@Test
	void with_future_data_then_throw_exception() {
		long epochSuperior = System.currentTimeMillis() + 10000000;
		Assertions.assertThrows(IllegalArgumentException.class, () -> {
			new ValidatorDetectorParamsImpl().validate("80.238.9.179," + epochSuperior + ",SIGNIN_SUCCESS,Will.Smith");
		});
	}
	
	@Test
	void with_incorrect_action_then_throw_exception() {
		Assertions.assertThrows(IllegalArgumentException.class, () -> {
			new ValidatorDetectorParamsImpl().validate("80.238.9.179,133612947,actionErroneo,Will.Smith");
		});
	}
	
	@Test
	void with_incorrect_user_format_then_throw_exception() {
		Assertions.assertThrows(IllegalArgumentException.class, () -> {
			new ValidatorDetectorParamsImpl().validate( "80.238.9.179,133612947,SIGNIN_SUCCESS,userErroneo");
		});
	}
	@Test
	void with_correct_param_then_not_throw_exception() {
		Assertions.assertDoesNotThrow(() -> {
			new ValidatorDetectorParamsImpl().validate( "80.238.9.179,133612947,SIGNIN_SUCCESS,Will.Smith");
		});
	}
}
