package com.hotelbeds.supplierintegrations.hackertest.detector;

import com.hotelbeds.supplierintegrations.hackertest.detector.validators.ValidatorDetectorParams;
import com.hotelbeds.supplierintegrations.hackertest.detector.validators.ValidatorDetectorParamsImpl;
import com.hotelbeds.supplierintegrations.hackertest.detector.validators.reviewers.SuspiciousActivityReviewer;
import com.hotelbeds.supplierintegrations.hackertest.detector.validators.reviewers.SuspiciousActivityReviewerImpl;
import com.hotelbeds.supplierintegrations.hackertest.detector.vo.LoginData;
import com.hotelbeds.supplierintegrations.hackertest.detector.vo.LoginResult;

import lombok.extern.slf4j.Slf4j;

@Slf4j
/**
 * My hacker detector
 */
public class HackerDetectorImpl implements HackerDetector {
	private final ValidatorDetectorParams parametersValidator;
	private final SuspiciousActivityReviewer suspiciousActivityController;

	public static void main(String[] args) {
		validateNumberOfParams(args);
		new HackerDetectorImpl(new ValidatorDetectorParamsImpl(), new SuspiciousActivityReviewerImpl())
				.parseLine(args[0]);
	}

	public HackerDetectorImpl(final ValidatorDetectorParams validadorParametros,
			final SuspiciousActivityReviewer suspiciousActivityController) {
		this.parametersValidator = validadorParametros;
		this.suspiciousActivityController = suspiciousActivityController;
	}

	@Override
	public String parseLine(String line) {
		parametersValidator.validate(line);
		LoginData loginData = recuperarDatosOperacion(line);
		if (isSuspiciousActivityIdentified(loginData)) {
			return generateSuspiciousOutput(loginData);
		} else {
			return generateNotSuspiciousOutput();
		}
	}

	private boolean isSuspiciousActivityIdentified(LoginData loginData) {
		if (!isLoginFailure(loginData)) {
			return false;
		} else {
			return suspiciousActivityController.isSuspiciousActivity(loginData);
		}
	}

	private String generateSuspiciousOutput(LoginData loginData) {
		return loginData.getIp();
	}

	private String generateNotSuspiciousOutput() {
		return null;
	}

	private boolean isLoginFailure(LoginData loginData) {
		return LoginResult.FAILURE.equals(loginData.getResult());
	}

	private LoginData recuperarDatosOperacion(String pLogLine) {
		LoginData loginData = new LoginData();
		String[] dataLog = pLogLine.split(",");
		loginData.setIp(dataLog[0]);
		loginData.setMoment(Long.valueOf(dataLog[1]));
		loginData.setResult(LoginResult.create(dataLog[2]));
		loginData.setUser(dataLog[3]);
		return loginData;
	}

	private static void validateNumberOfParams(String[] args) {
		if (args == null || args.length != 1) {
			log.error("No se han recibido el número correcto de argumentos");
			throw new IllegalArgumentException();
		}
	}
}
