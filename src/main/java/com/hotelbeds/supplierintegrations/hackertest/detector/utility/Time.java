package com.hotelbeds.supplierintegrations.hackertest.detector.utility;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Utility for operate with datetime in RFC2822 format
 */
public interface Time {
	public static final String PATTERN_RFC2822 = "EEE, dd MMM yyyy HH:mm:ss Z";
	
	/**
	 * Diff in minutes
	 * @param initialMoment Timestamp in format RFC2822
	 * @param finalMoment Timestamp in format RFC2822
	 * @return minutos transcurridos
	 */
	public static long getDiffInMinutes(final String initialMoment, final String finalMoment) {
		try {
			final SimpleDateFormat dateFormater = new SimpleDateFormat(PATTERN_RFC2822);
			Date firstDate = dateFormater.parse(initialMoment);
			Date secondDate = dateFormater.parse(finalMoment);
			long diffInMillies = Math.abs(secondDate.getTime() - firstDate.getTime());
			long diffMinutes = TimeUnit.MINUTES.convert(diffInMillies, TimeUnit.MILLISECONDS);
			return diffMinutes;
		} catch (ParseException e) {
			throw new RuntimeException("Error with format of date");
		} 
	}
}
