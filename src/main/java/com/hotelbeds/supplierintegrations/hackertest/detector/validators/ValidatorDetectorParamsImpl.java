package com.hotelbeds.supplierintegrations.hackertest.detector.validators;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import lombok.extern.slf4j.Slf4j;

/**
 * My implementation of validator
 */
@Slf4j
public class ValidatorDetectorParamsImpl implements ValidatorDetectorParams {

	private static final String DESCRIPTION_DATE_NOT_VALID = "Date not valid";
	private static final List<String> VALID_ACTIONS = Arrays.asList("SIGNIN_SUCCESS", "SIGNIN_FAILURE");
	private static final String PATTERN_USER_CODE = "\\w+\\.\\w+";
	@Override
	public boolean validate(String mainParameter) {
		String[] logData = mainParameter.split(",");
		if (logData.length != 4) {
			throw new IllegalArgumentException();
		}
		validateParamIp(logData[0]);
		validateParamDate(logData[1]);
		validateParamAction(logData[2]);
		validateParamUser(logData[3]);
		return true;
	}

	private void validateParamUser(String usuario) {
		if (!usuario.matches(PATTERN_USER_CODE)) {
			log.error("User format not valid: " + usuario);
			throw new IllegalArgumentException("User format not valid");
		}
	}

	private void validateParamAction(String action) {
		if (!VALID_ACTIONS.contains(action)) {
			log.error("Action not valid: " + action);
			throw new IllegalArgumentException("Action not valid");
		}
	}

	private void validateParamDate(String fecha) {
		try {
			long fechaEpoch = Long.valueOf(fecha);
			long fechaActual = System.currentTimeMillis();
			if (fechaActual < fechaEpoch) {
				log.error("Date not valid: " + fecha);
				throw new IllegalArgumentException(DESCRIPTION_DATE_NOT_VALID);
			}
		} catch (NumberFormatException e) {
			log.error("Date not valid: " + fecha);
			throw new IllegalArgumentException(DESCRIPTION_DATE_NOT_VALID);
		}

	}

	private void validateParamIp(String ip) {
		String zeroTo255 = "(\\d{1,2}|(0|1)\\" + "d{2}|2[0-4]\\d|25[0-5])";
		String regex = zeroTo255 + "\\." + zeroTo255 + "\\." + zeroTo255 + "\\." + zeroTo255;
		Pattern p = Pattern.compile(regex);
		if (ip == null || !p.matcher(ip).matches()) {
			log.error("Ip not valid: " + ip);
			throw new IllegalArgumentException("Ip not valid");
		}
	}


}
