package com.hotelbeds.supplierintegrations.hackertest.detector.validators.reviewers;

import com.hotelbeds.supplierintegrations.hackertest.detector.vo.LoginData;

/**
 * Operations of suspicious activity reviewers
 *
 */
public interface SuspiciousActivityReviewer {
	/**
	 * Review if is suspicious activity
	 * @param loginData Login data
	 * @return true / false 
	 */
	boolean isSuspiciousActivity(LoginData loginData);

}
