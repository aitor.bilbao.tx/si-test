package com.hotelbeds.supplierintegrations.hackertest.detector.vo;

import lombok.Data;

@Data
/**
 * Login Data
 */
public class LoginData {
	private String ip;
	private long moment;
	private LoginResult result;
	private String user;
}
