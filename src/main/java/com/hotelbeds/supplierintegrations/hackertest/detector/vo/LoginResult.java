package com.hotelbeds.supplierintegrations.hackertest.detector.vo;

/**
 * Tipos posibles de operacion
 */
public enum LoginResult {
	SUCCESS, FAILURE;
	
	public static LoginResult create(String resultCode) {
		if (resultCode.equals("SIGNIN_SUCCESS")) {
			return SUCCESS;
		} else {
			return FAILURE;
		}
	}
}
