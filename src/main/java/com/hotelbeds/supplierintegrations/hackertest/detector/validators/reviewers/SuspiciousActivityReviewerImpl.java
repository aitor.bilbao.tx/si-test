package com.hotelbeds.supplierintegrations.hackertest.detector.validators.reviewers;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hotelbeds.supplierintegrations.hackertest.detector.utility.Time;

import com.hotelbeds.supplierintegrations.hackertest.detector.vo.LoginData;

/**
 * My implementation
 */
public class SuspiciousActivityReviewerImpl implements SuspiciousActivityReviewer {
	public static final Integer MAX_NUMBER_OF_FILURED_LOGINS = 5;
	public static final Integer DIFF_IN_MINUTES = 5;
	private static Map<String, List<Long>> failuredOperations = new HashMap<>();
	private static final SimpleDateFormat dateFormater = new SimpleDateFormat(Time.PATTERN_RFC2822);
	
	@Override
	public boolean isSuspiciousActivity(LoginData loginData) {
		updateFailuredOperations(loginData);
		return testIfReachMaxNumberOfFailuredLogins(loginData);
	}
	
	private boolean testIfReachMaxNumberOfFailuredLogins(LoginData datosOperacion) {
		List<Long> failuredMomentsOfIp = failuredOperations.get(datosOperacion.getIp());
		if (failuredMomentsOfIp == null || failuredMomentsOfIp.size() < MAX_NUMBER_OF_FILURED_LOGINS) {
			return false;
		}
		return (testIfOnTime(failuredMomentsOfIp));
	}

	private boolean testIfOnTime(List<Long> failuredMomentsOfIp) {
		List<Long> listWithoutLastLogin = failuredMomentsOfIp.subList(0, failuredMomentsOfIp.size() -1);
		Long lastMomentOfFailuredLogin = failuredMomentsOfIp.get(failuredMomentsOfIp.size() - 1);
		String lastMomentFormated = dateFormater.format(new Date(lastMomentOfFailuredLogin));
		int countOnTime = 1;
		for (Long momentFailuredLogin : listWithoutLastLogin) {
			//Esta parte no me gusta pero como hay que usar el método de comparación de fechas en timestamp
			String momentFormated = dateFormater.format(new Date(momentFailuredLogin));
			if (Time.getDiffInMinutes(lastMomentFormated, momentFormated) <= DIFF_IN_MINUTES) {
				countOnTime++;
			}
		}
		return countOnTime >= MAX_NUMBER_OF_FILURED_LOGINS;
	}

	private void updateFailuredOperations(LoginData loginData) {
		List<Long> failuredMomentsOfIp = failuredOperations.get(loginData.getIp());
		if (failuredMomentsOfIp == null) {
			failuredMomentsOfIp = new ArrayList<>();
		}
		failuredMomentsOfIp.add(loginData.getMoment());
		failuredOperations.put(loginData.getIp(), failuredMomentsOfIp);
	}
	

}
