package com.hotelbeds.supplierintegrations.hackertest.detector;

/**
 * Operations of hacker detectors implementations
 */
public interface HackerDetector {
    String parseLine(String line);
}

