package com.hotelbeds.supplierintegrations.hackertest.detector.validators;

/**
 * Operations of parameters validators
 */
public interface ValidatorDetectorParams {
	/**
	 * Validate the main Parameter
	 * @param mainParameter Parameter
	 */
	public boolean validate(String mainParameter);
}
